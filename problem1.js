const fs = require('fs');
const path = require('path');

const createDirectory = (directoryName) => {
    return new Promise((resolve, reject) => {
        fs.mkdir(directoryName, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

const fileCreation = (filename) => {
    return new Promise((resolve, reject) => {
        fs.open(filename, 'w', (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        })
    })
}

const getFilenames = (directory) => {
    return new Promise((resolve, reject) => {
        fs.readdir(directory, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

const deleteFiles = (directory, filenames) => {
    return filenames.reduce((acc, file) => {
        return acc.then(() => {
            return deleteFile(directory + file);
        })
    }, Promise.resolve());
}

const deleteFile = (filename) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => fs.unlink(filename, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log(`Deleting ${filename}`)
                resolve();
            }
        }), 2000)
    });
}

const directoryName = path.resolve(__dirname, './JSON_Files');

createDirectory(directoryName)
    .then(() => fileCreation(`${directoryName}/file1.json`))
    .then(() => fileCreation(`${directoryName}/file2.json`))
    .then(() => fileCreation(`${directoryName}/file3.json`))
    .then(() => fileCreation(`${directoryName}/file4.json`))
    .then(() => fileCreation(`${directoryName}/file5.json`))
    .then(() => getFilenames(directoryName))
    .then((data) => deleteFiles(`${directoryName}/`, data))
    .then(() => console.log('Successfully deleted all the files'))
    .catch((err) => {
        console.log(err);
    })