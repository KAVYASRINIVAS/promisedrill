const fs = require('fs');
const { resolve } = require('path');
const path = require('path');

const readDataFromFile = (filename) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

const convertToUpperCase = (data) => {
    return new Promise((resolve, reject) => {
        let ucase = data.toString().toUpperCase();
        resolve(ucase);
    })
}

const writeDataIntoFile = (file, content) => {
    fs.writeFile(file, content, (err) => {
        if (err) {
            reject(err);
        } else {
            resolve();
        }
    })

}

const convertToLowerCase = (data) => {
    return new Promise((resolve, reject) => {
        let lcase = data.toString().toLowerCase();
        resolve(lcase);
    })
}

const appendDataToFile = (filename, data) => {
    return new Promise((resolve, reject) => {
        fs.appendFile(filename, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        })

    })
}

const splitIntoSentences = (data) => {
    let newData = data.toString().split(/[!.\n]/);
    let sentence = [];
    newData = newData.filter(sentence => sentence);
    for (let i = 0; i < newData.length; i++) {
        if (newData[i] === ' ') {
            continue;
        }
        let str = newData[i].trim();
        sentence.push(str.charAt(0).toUpperCase() + str.substring(1) + '.');
    }
    return sentence;
}

const deleteFiles = (data) => {
    let filenames = data.split('\n');
    return new Promise((resolve, reject) => {
        for (let i = 0; i < filenames.length; i++) {
            fs.unlink(filenames[i], function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        }
    });
}

const sortData=(data,filename)=>{
    let files=data.split('\n');
    for(let i=0;i<files.length;i++){
        readDataFromFile(files[i])
        .then((data) => {
            let content=data.split('\n').sort();
            content.forEach(line => {
                appendDataToFile(filename,`${line}\n`);
            })
            
        })
    }
}

const inputFile = path.resolve(__dirname, './lipsum.txt');
const ucaseFile = path.resolve(__dirname, './upperCaseFile.txt');
const lcaseFile = path.resolve(__dirname, './lowerCaseFile.txt');
const sentences = path.resolve(__dirname, './sentencesFile.txt');
const sortFile = path.resolve(__dirname, './sortedDataFile.txt');
const filenamesContainer = path.resolve(__dirname, './filenames.txt');

readDataFromFile(inputFile)
    .then((data) => convertToUpperCase(data))
    .then((data) => writeDataIntoFile(ucaseFile, data))
    .then(() => writeDataIntoFile(filenamesContainer, ucaseFile))
    .then(() => readDataFromFile(ucaseFile))
    .then((data) => convertToLowerCase(data))
    .then((data) => writeDataIntoFile(lcaseFile, data))
    .then(() => appendDataToFile(filenamesContainer, `\n${lcaseFile}`))
    .then(() => readDataFromFile(lcaseFile))
    .then((data) => splitIntoSentences(data))
    .then((data) => {
        return data.reduce((acc,currentLine) =>{
            return acc.then(()=>{
                return appendDataToFile(sentences, `${currentLine}\n`);
            })
        },Promise.resolve())
    })
    .then(() => appendDataToFile(filenamesContainer, `\n${sentences}`))
    .then(() => readDataFromFile(filenamesContainer))
    .then((data)=>sortData(data,sortFile))
    .then(()=>appendDataToFile(filenamesContainer,`\n${sortFile}`))
    .then(()=>readDataFromFile(filenamesContainer))
    .then((data) => deleteFiles(data))
    .catch((err) => console.log(err));